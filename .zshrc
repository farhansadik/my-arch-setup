# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/farhansadik/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


# build for arch and arch based distro

alias install='sudo pacman -S'
alias remove='sudo pacman -Rs'
alias sync='sudo pacman -Syy'
alias upgrade='sudo pacman -Syu'
alias clean-cache='sudo pacman -Scc'
alias please='sudo'
alias make-package='makepkg -si'

# Alias of git 
alias commit='git commit'
alias status='git status'
alias branch='git branch'
alias push='git push'
alias pull='git pull'
alias add='git add' 

# configuration-settings
alias ccli-help='less ~/.config/ccli/help.txt'
alias ccli-settings='not installed'

# tagspaces 
#alias tagspaces=`~/Application/tagspaces/./tagspaces --portable`;
# alias tagspaces=`~/Application/tagspaces/./tagspaces`;

function tagspaces() {

	loc=$HOME/Application/tagspaces/; file=launch.sh;
	if cd $loc; then { if sh $file; then cd $HOME; fi; }; fi

}


# corona status 
alias corona='curl https://corona-stats.online'

# Hotspot 
# ask password of hotspot 
# alias hotspot-start='nmcli connection up farhansadik-hotspot --ask' 

# alias hotspot-start='nmcli connection up MyNet'
# alias hotspot-stop='nmcli connection down MyNet'

# pppoe start/stop
alias pstart='sudo pppoe-start'
alias pstop='sudo pppoe-stop'

function hotspot-start() {
    # this function is created to start hotspot 
	clear && tput bel; echo;
	printf "==================================\n";
	printf "BroadBand Connection\n";
	printf "==================================\n"
	pppoe-status; echo
	printf "==================================\n"
	printf "Starting Hotspot\n";
	printf "==================================\n"
	if nmcli connection up MyNet && echo; then {
		printf "==================================\n"
		printf "Device Status\n";
		printf "==================================\n"
		nmcli connection show && tput bel; 
	}; else {
		printf "Failed to run hotpot\nPlease contact with administrator\n";
	}; fi
	
	echo; printf "Program By\n";
	printf "Farhan Sadik\n" && echo;
}

function hotspot-stop() {
    # this function is created for stop hotspot 
	clear && tput bel; echo 
	printf "==================================\n";
	printf "BroadBand Connection\n";
	printf "==================================\n"
	pppoe-status; echo;
	printf "==================================\n"
	printf "Stoping Hotspot\n";
	printf "==================================\n"
	if nmcli connection down MyNet && echo; then {
		printf "==================================\n"
		printf "Device Status\n";
		printf "==================================\n"
		#nmcli connection show && tput bel; 
		printf "Connection successfully deactivated\n" && tput bel; 
	}; else {
		printf "\nFailed to stop hotpot\nPlease contact with administrator\n";
	}; fi
	echo; printf "Program By\n";
	printf "Farhan Sadik\n" && echo;

}

# created by farhan sadik

