# My Linux Wiki 

I've created this repository for storing my data into internet as backup. This is only for my personal use. If you need any data/info of mine you can use it. You can modify it if you want. Please do not use any data for commercial.  <br>
Even you can fork it and help me to get something better. You're welcome. <br>

## My Current System 
| x | y           |
|------------------|---------------------|
| Operating System | Dual Boot           |
| OS[0]            | Arch Linux          |
| OS[1]            | Windows10 Pro 22H2  |
| Primary Desktop  | Gnome               |



## Wiki 

> For more info please take a look [**wiki page**](https://gitlab.com/farhansadik/my_arch-setup/wikis/home). I've stored lot of my informative research data.


## My Regular Linux Packages List 

**Regular Application**

| Category   | Name and Address |
|------------------|---------------------|
| text editor   |    mousepad, gedit|
| pdf reader     |   okular  |
| video player   |   vlc  |
| audio player   |   clemintine |
| browser        |   firefox |
| download manager |  xdman https://aur.archlinux.org/xdman.git |
| calculator 	    |  gnome-calculator |
| file manager     | pcmanfm, dolphin, pantheon-file (for extra usage beside nautilus) |
| image viewer 	 | nomacs, gnome-image-viewer |
| package manager |  pamac https://aur.archlinux.org/pamac-aur.git|
| word processing |  wps office https://aur.archlinux.org/wps-office.git |
| torrent client  |  deluge/qbittorrent |
| archive manager |   ark  |
| cleaning app 	|  stacer https://aur.archlinux.org/stacer.git  |
| markdown doc| [https://aur.archlinux.org/packages/apostrophe](https://aur.archlinux.org/packages/apostrophe)|

**IDE** 
* sublime text (package=https://aur.archlinux.org/sublime-text-dev.git) 
* geany 

**Gnome Plugins / extensions**


> ## Note
> Before add any extention through browser you'll need to install `gnome-shell-extensions` package! 

* dash to dock (https://extensions.gnome.org/extension/307/dash-to-dock/)
* dash to panel (https://extensions.gnome.org/extension/1160/dash-to-panel/) 
* netspeed (https://extensions.gnome.org/extension/104/netspeed/)

**Essential Application**

* gparted 
* simple screen recorder (package=https://aur.archlinux.org/simplescreenrecorder-git.git)
* grub-customizer 
* terminator
* gitkraken (package=https://aur.archlinux.org/gitkraken.git )
* tagspaces (package=https://aur.archlinux.org/tagspaces.git) // better use from github
* typora (package=https://aur.archlinux.org/typora.git)
* FileZilla
* kde-connect
* virtualbox
* woeusb (package=https://aur.archlinux.org/woeusb.git)
* gcolor3 (color picker)
* scrcpy (package=https://aur.archlinux.org/scrcpy.git)
* imagewriter (package=https://aur.archlinux.org/imagewriter.git) (suse image writer)

**Command line style tools**
```
git zsh sl figlet bat zip unzip unrar htop lolcat cmatrix screenfetch neofetch vim nano

glances - monitoring cli based app
man // man manual 
ffmpegthumbnailer // to view thumbnails in nautilus 
nethogs // network monitoring tool
```

## File List
 * `gnome_package-list.txt` - packages list of gnome
 * `my_conky_script` - it's a conky script
 * `.fonts` - my regular fonts
 * `wifi` - my wifi driver

Farhan Sadik <br>
Square Development Group
